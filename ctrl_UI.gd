extends 'res://PopupGUI.gd'

var core
sync var heat = 0.0
sync var capacity = 10.0
sync var rad_capacity = 10.0
sync var heat_coeff = 1500
sync var temp = 600
sync var cooling_rate = 0.0
sync var cool_coeff = 0.001
sync var pwr = 0.0
sync var ctrl = 0.0
sync var pump = 0.0
sync var rad_temp = 300
sync var rad_cool = 0.0
sync var eff = 0.0

var rate_coeff = 0.01
var decay_coeff = 0.005

func _ready():
	core = get_parent().get_node("Core_UI")

func _process(delta):
	$ctrl/txt.text = str("Control Rod Depth: ", ctrl)
	$ctrl.value = ctrl
	$pump/txt.text = str("Pump Rate: ", pump)
	$heat.value = heat
	$heat/txt.text = str("Core Heat: ", stepify(heat, 0.1), " MW")
	$temp.value = temp
	$temp/txt.text = str("Core Temp: ", stepify(temp, 0.1), " K")
	$cool.value = cooling_rate
	$cool/txt.text = str("Cooling Rate: ", stepify(cooling_rate, 0.1), " MW")
	$eff.value = eff
	$eff/txt.text = str("Efficiency: ", stepify(eff, 0.1), " %")
	$rad.value = rad_temp
	$rad/txt.text = str("Radiator Temp: ", stepify(rad_temp, 0.1), " K")
	$rad_cool.value = rad_cool
	$rad_cool/txt.text = str("Radiator Cooling: ", stepify(rad_cool, 0.1), " MW")
	$pwr.value = pwr
	$pwr/txt.text = str("Power: ", stepify(pwr, 0.1), " MW")

func _physics_process(delta):
	core.rate = (100-ctrl)/100 * rate_coeff * max((core.fuel - core.waste), 0)
	heat = heat_coeff * (core.rate + decay_coeff*core.waste)
	cooling_rate = (pump+5) * cool_coeff * (temp - rad_temp)
	temp += delta * (heat - cooling_rate) / capacity
	rad_cool = 0.1 * pow(rad_temp*0.01, 4)
	rad_temp += delta * (cooling_rate - rad_cool) / rad_capacity
	eff = 100 - 100*rad_temp/temp
	pwr = eff/100 * cooling_rate

func _on_ctrl_value_changed(value):
	rset("ctrl", value)

func _on_SCRAM():
	rset("ctrl", 100)

func _on_pump_value_changed(value):
	rset("pump", value)
