extends KinematicBody2D

const MOTION_SPEED = 250.0

puppet var puppet_pos = Vector2()
puppet var puppet_motion = Vector2()

export var stunned = false
var action = false
var suited = false
var busy = false
sync var item = ""

sync func set_item(got):
	item = got

sync func wear_suit(by_who):
	suited = true
	$suit_sprite.show()
	$sprite.hide()

sync func unwear_suit(by_who):
	suited = false
	$suit_sprite.hide()
	$sprite.show()
	
var current_anim = ""
var bomb_index = 0

func _input(event):
	if Input.is_action_pressed("set_bomb"):
		action = true
	if Input.is_action_pressed("grab"):
		pass
		
func _process(delta):
	$item.text = item
		
func _physics_process(_delta):
	var motion = Vector2()

	if is_network_master():
		if Input.is_action_pressed("move_left"):
			motion += Vector2(-1, 0)
		if Input.is_action_pressed("move_right"):
			motion += Vector2(1, 0)
		if Input.is_action_pressed("move_up"):
			motion += Vector2(0, -1)
		if Input.is_action_pressed("move_down"):
			motion += Vector2(0, 1)

		if stunned:
			action = false
			motion = Vector2()
			
		if action:
			if get_tree().get_root().get_node("World/rooms/core").overlaps_body(self):
				get_tree().get_root().get_node("World/UI/Core_UI").activate(self)
				busy = true
			elif get_tree().get_root().get_node("World/rooms/fuel_assembler").overlaps_body(self):
				get_tree().get_root().get_node("World/UI/Fuel_UI").activate(self)
				busy = true
			elif get_tree().get_root().get_node("World/rooms/ctrl_room").overlaps_body(self):
				get_tree().get_root().get_node("World/UI/ctrl_UI").activate(self)
				busy = true
			elif get_tree().get_root().get_node("World/rooms/fuel_reproc").overlaps_body(self):
				get_tree().get_root().get_node("World/UI/Reproc_UI").activate(self)
				busy = true
			elif get_tree().get_root().get_node("World/rooms/nuke_maker").overlaps_body(self):
				get_tree().get_root().get_node("World/UI/NukeMaker_UI").activate(self)
				busy = true
			elif get_tree().get_root().get_node("World/rooms/missiles").overlaps_body(self):
				get_tree().get_root().get_node("World/UI/Nuke_UI").activate(self)
				busy = true
			elif get_tree().get_root().get_node("World/rooms/cooling").overlaps_body(self):
				get_tree().get_root().get_node("World/UI/Turbine_UI").activate(self)
				busy = true
			elif get_tree().get_root().get_node("World/rooms/airlock_top").overlaps_body(self):
				rpc("wear_suit", get_tree().get_network_unique_id())
				position.y -= 30+13
			elif get_tree().get_root().get_node("World/rooms/airlock_top_out").overlaps_body(self):
				rpc("unwear_suit", get_tree().get_network_unique_id())
				position.y += 30+13
			else:
				for body in get_tree().get_nodes_in_group("U"):
					if body.overlaps_body(self):
						rpc("set_item", "U Ore")

		rset("puppet_motion", motion)
		rset("puppet_pos", position)
	else:
		position = puppet_pos
		motion = puppet_motion
	
	if busy:
		motion = Vector2()
	
	var new_anim = "standing"
	if motion.y < 0:
		new_anim = "walk_up"
	elif motion.y > 0:
		new_anim = "walk_down"
	elif motion.x < 0:
		new_anim = "walk_left"
	elif motion.x > 0:
		new_anim = "walk_right"
	if suited:
		get_node("anim").play("suited")
	
	if stunned:
		new_anim = "stunned"

	if new_anim != current_anim:
		current_anim = new_anim
		get_node("anim").play(current_anim)

	move_and_slide(motion * MOTION_SPEED)
	if not is_network_master():
		puppet_pos = position # To avoid jitter
	action = false

puppet func stun():
	stunned = true

master func stun_all(_by_who):
	if stunned:
		return
	rpc("stun") # Stun puppets
	stun() # Stun master - could use sync to do both at once

func set_player_name(new_name):
	get_node("label").set_text(new_name)

func _ready():
	stunned = false
	puppet_pos = position
