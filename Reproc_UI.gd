extends 'res://PopupGUI.gd'

sync var waste = 3.0
sync var fuel = 2.5
sync var Pu = 0.0
sync var capacity = 6.0
sync var Pu_on = false

func _process(delta):
	$fuel.value = fuel
	$Pu.value = Pu
	$fuel_txt.text = str(stepify(fuel,0.01), " kg Fuel\n",
		stepify(Pu,0.01)," kg Pu")
	$waste.value = waste
	$U_txt.text = str(stepify(waste,0.01), " kg")

func _physics_process(delta):
	if is_network_master():
		if waste >= 0.1*delta and fuel <= capacity - 0.1*delta:
			rpc("process", delta)

sync func process(delta):
	waste -= 0.1*delta
	if Pu_on:
		Pu += 0.1*delta
	else:
		fuel += 0.1*delta


func _on_insert_pressed():
	if waste <= capacity and user.item == "Waste":
		rset("waste", waste + 1)
		user.rpc("set_item", "")

func _on_take_Pu_pressed():
	if fuel >= 1 and user.item == "":
		rset("Pu", Pu - 1)
		user.rpc("set_item", "Pu")

func _on_Pu_Toggle_pressed():
	var toggle = !Pu_on
	rpc("toggle_Pu",toggle)
sync func toggle_Pu(toggle):
	$Pu_Toggle.toggle_mode = toggle
	$Arrow.rotation_degrees = -45 + 90*int(toggle)
	Pu_on = toggle

func _on_take_fuel():
	if fuel >= 1 and user.item == "":
		rset("fuel", fuel - 1)
		user.rpc("set_item", "Rod")
