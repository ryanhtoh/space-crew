extends Node2D

var target
var speed = 100
var dmg = 10

func _ready():
	$audio.play()
	$explosion.stop()
	$explosion.hide()

func _physics_process(delta):
	position += Vector2(speed*delta, 0).rotated(rotation)

func _on_Area2D_area_entered(area):
	target = area.get_parent()
	if target.is_in_group("enemies"):
		$explosion.show()
		$rocket.hide()
		$explosion.play()
		target.hp -= dmg
		if target.hp <= 0:
			target.queue_free()

func _on_explosion_animation_finished():
	#var targets = $Area2D.get_overlapping_areas()
	queue_free()

func _on_Timer_timeout():
	$audio.stop()
