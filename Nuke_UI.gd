extends 'res://PopupGUI.gd'

var rng = RandomNumberGenerator.new()
sync var ammo = 6

func _ready():
	$nukes.value = ammo
	rng.randomize()

func _physics_process(delta):
	$nukes.value = ammo
	if is_network_master():
		time += delta
		if time >= 15:
			spawn_enemy()
			time -= 15

func spawn_enemy():
	var dir = rng.randi_range(1,4)
	var x
	var y
	match dir:
		1:
			x = rng.randf_range(0, rect_size.x)
			y = 0
		2:
			x = 0
			y = rng.randf_range(0, rect_size.y)
		3:
			x = rng.randf_range(0, rect_size.x)
			y = rect_size.y
		4:
			x = rect_size.x
			y = rng.randf_range(0, rect_size.y)
	rpc("create_enemy", x, y)

sync func create_enemy(x, y):
	var spawn_scene = load("res://enemy.tscn")
	var spawn = spawn_scene.instance()
	add_child(spawn)
	spawn.add_to_group("enemies")
	spawn.position = Vector2(x, y)
	spawn.target = $ship


func _on_insert_pressed():
	if user.item == "Nuke":
		rset("ammo", ammo + 1)
		user.rpc("set_item", "")

func _on_remove_pressed():
	if user.item == "":
		rset("ammo", ammo -1)
		user.rpc("set_item", "Nuke")

func _on_Nuke_UI_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		print("input1")
		if ammo > -990:
			rpc("create_nuke", event.position, get_global_mouse_position())
sync func create_nuke(position, mouse):
	ammo -= 1
	var spawn_scene = load("res://rocket.tscn")
	var spawn = spawn_scene.instance()
	add_child(spawn)
	spawn.add_to_group("nukes")
	spawn.position = $ship.position
	spawn.look_at(mouse)
