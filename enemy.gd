extends Node2D

var target
var speed = 10
var hp = 10

func _physics_process(delta):
	position += position.direction_to(target.position)*speed*delta
	look_at(target.position + get_parent().rect_position)
