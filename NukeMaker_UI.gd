extends 'res://PopupGUI.gd'

sync var A = 3.0
sync var prog = 0.0
sync var B = 2
var A_cap = 6
var B_cap = 6
var A_rate = 0.1
var B_rate = 0.1

func _process(delta):
	$A.value = A
	$A_txt.text = str(stepify(A,0.01), " kg")
	$B.value = B
	$B_txt.text = str(stepify(B,0.01), " Nukes")
	$prog.value = prog

func _physics_process(delta):
	if is_network_master():
		if A >= A_rate*delta and B < B_cap:
			rpc("process", delta)

sync func process(delta):
	#A -= A_rate*delta
	prog += B_rate*delta
	if prog >= 1.0:
		prog = 0.0
		A -= 1
		B += 1

func _on_insert_pressed():
	if user.get_node("item").text == "Pu":
		rset("A",A+1)
		user.rpc("set_item", "")

func _on_remove_pressed():
	if B >= 1 and user.get_node("item").text == "":
		rset("B",B-1)
		user.rpc("set_item", "Nuke")
