extends 'res://PopupGUI.gd'

var ctrl
sync var fuel = 5.0
sync var waste = 2.0
sync var capacity = 10.0
sync var rate = 0.01

func _ready():
	ctrl = get_parent().get_node("ctrl_UI")

func _physics_process(delta):
	fuel -= delta*rate
	waste += delta*rate
	$fuel.value = fuel+waste
	$waste.value = waste
	
	$fuel_txt.text = str("Fuel: ",stepify(fuel,0.01)," kg",
		"\nWaste: ",stepify(waste,0.01)," kg",
		"\nCapacity Used: ",stepify(fuel+waste,0.01)," kg / ",capacity," kg")
	#$temp_txt.text = str(stepify(temperature,0.01), " K")

func _on_insert_pressed():
	if user.get_node("item").text == "Rod":
		rset("fuel", fuel + 1)
		user.rpc("set_item", "")

func _on_remove_pressed():
	if user.get_node("item").text == "" and waste >= 1.0:
		rset("waste", waste - 1)
		user.rpc("set_item", "Waste")
