extends Node2D

func _ready():
	spawn_u()

sync func spawn_u():
	var spawn_scene = load("res://U.tscn")
	var spawn = spawn_scene.instance()
	get_node("/root/World/ore").add_child(spawn)
	spawn.add_to_group("U")
	spawn.position = Vector2(1000, rand_range(-140,-340))
	spawn.motion = Vector2(0, 0)
