extends 'res://PopupGUI.gd'

sync var U = 3.0
sync var fuel = 2.5

func _process(delta):
	$fuel.value = fuel
	$fuel_txt.text = str(stepify(fuel,0.01), " kg")
	$U.value = U
	$U_txt.text = str(stepify(U,0.01), " kg")

func _physics_process(delta):
	if is_network_master():
		if U >= 0.1*delta and fuel <= 6 - 0.1*delta:
			rpc("process", delta)

sync func process(delta):
	U -= 0.1*delta
	fuel += 0.1*delta

func _on_insert_pressed():
	if user.item == "U Ore":
		rset("U", U + 1)
		user.rpc("set_item", "")

func _on_remove_pressed():
	if fuel >= 1 and user.item == "":
		rset("fuel", fuel - 1)
		user.rpc("set_item", "Rod")
